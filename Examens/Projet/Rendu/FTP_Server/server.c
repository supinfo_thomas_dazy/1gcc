#include "server.h"

// On inclus les librairies dont on a besoins
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <ftw.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

// Mettre ici le chemin complet du dossier 'files' � la racine du projet
const char cheminInitial[1000] = "/Users/thomasdazy/Documents/1._Supinfo/9._1GCC_-_C_language/Project/FTP_Server/files";

char *cheminActuel              = "";
char cheminIntermediaire[1000]  = "";
char cheminPrecedent[1000]      = "";
char *cheminNouveau             = "";
int nombreDeClient              = 0;
int connectionTerminee          = 0;

SOCKET init(void){

    #ifdef WIN32
        WSADATA wsa;
        int err = WSAStartup(MAKEWORD(2, 2), &wsa);
        if(err < 0){
            printf("Load Dll fail\n");
            exit(EXIT_FAILURE);
        }
    #endif
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    SOCKADDR_IN serv_addr;

    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(1337);
    serv_addr.sin_family = AF_INET;

    bind(sock,(SOCKADDR *) &serv_addr, sizeof(serv_addr));
    return sock;
}

void finish(void){
    #ifdef WIN32
        WSACleanup();
    #endif
}

SOCKET start(SOCKET sock) {
    // On r�cup�re le chemin initial � l'initialisation du programme
    cheminActuel = cheminInitial;
    strcpy(cheminIntermediaire, cheminActuel);

    if(listen(sock, 100) < 0) {
        printf("Erreur d'ouverture des ports\n");
        exit(-1);
    }
    char buffer[1024],tmpBuffer[1024];
    /* the index for the array */
    int nbClients = 0;//nb of client
    int set = 0;//select result
    int max = sock;
    int i;

    Client clients[100];

    fd_set fs;

    while(1) {
        i = 0;

        FD_ZERO(&fs);
        FD_SET(sock, &fs);

        for(i = 0; i < nbClients; i++){
            FD_SET(clients[i].sock, &fs);
        }

        set = select(max + 1, &fs, NULL, NULL, NULL);
        if(set < 0){
            printf("Erreur de select\n");
        }
        else if(set  == 0){

        }

        else if(FD_ISSET(sock, &fs)){
            SOCKADDR_IN cli_addr;
            int sinsize = sizeof(cli_addr);
            SOCKET cli = accept(sock, (SOCKADDR *) &cli_addr, &sinsize);

            Client c;
            c.sock = cli;
            if(client_recv(cli, c.name) < 0){

            }
            printf("Connecte: %s\n", c.name);

            max = cli > max ? cli : max;

            FD_SET(cli, &fs);

            clients[nbClients] = c;




            strncpy(tmpBuffer, "Server: ",sizeof(tmpBuffer));
            strncat(tmpBuffer, clients[nbClients].name, sizeof(tmpBuffer) - strlen(tmpBuffer) - 1);
            strncat(tmpBuffer, " est maintenant connecte", sizeof(tmpBuffer) - strlen(tmpBuffer) - 1);

            send_to_all(clients,clients[nbClients], nbClients, tmpBuffer);
            nbClients++;
            printf("%d clients connectes\n",nbClients);

            // Message d'accueil
            client_send(clients[nbClients - 1].sock, "Welcome to FTP\nYou can use help to list commands\n");
        }
        else{
            for(i = 0; i < nbClients; i++){
                if(FD_ISSET(clients[i].sock, &fs)){

                    if(client_recv(clients[i].sock, buffer) < 1){
                        printf("Deconnecte: %s \n",clients[i].name);

                        strncpy(buffer, clients[i].name, sizeof(buffer) );
                        strncat(buffer, " deconnecte !!!", sizeof(buffer) - strlen(buffer) - 1);

                        send_to_all(clients, clients[i], nbClients, buffer);
                        client_close(clients, i, &nbClients);
                    }
                    else{
                        printf("%s: %s\n", clients[i].name, buffer);
                        strncpy(tmpBuffer, clients[i].name, sizeof(tmpBuffer));
                        strncat(tmpBuffer, " : ", sizeof(tmpBuffer) - strlen(tmpBuffer) - 1);
                        strncat(tmpBuffer, buffer, sizeof(tmpBuffer) - strlen(tmpBuffer) - 1);
                        send_to_all(clients, clients[i], nbClients, tmpBuffer);
                    }
                    break;
                }
            }
        }

        char nombreconnecte[50];
        sprintf(nombreconnecte, "%i", nbClients);
        char messageOfNumberOfConnected[1000] = "";
        strcpy(messageOfNumberOfConnected, nombreconnecte);
        strcat(messageOfNumberOfConnected, " clients connectes");

        // Si l'utilisateur ecris 'help'
        if (strstr(buffer, "help") != NULL)
        {
            // On affiche la liste des commandes
            client_send(clients[i].sock, "list of commands");
            client_send(clients[i].sock, "- help: list all available commands");
            client_send(clients[i].sock, "- cd: change the current directory");
            client_send(clients[i].sock, "- get: download a file");
            client_send(clients[i].sock, "- ls: list files and directories");
            client_send(clients[i].sock, "- delete: delete a file or a directory");
            client_send(clients[i].sock, "- mkdir: create a directory");
            client_send(clients[i].sock, "- put: send a file");
            client_send(clients[i].sock, "- status: list numbers of connected users");
            client_send(clients[i].sock, "- quit: logout the client");
        }

        // Si l'utilisateur ecris 'ls'
        else if (strstr(buffer, "ls") != NULL)
        {
            // On initialise les chemins des fichiers
            DIR           *d;
            struct dirent *dir;

            DIR           *subdir;
            int testSiDossierEtSiExiste(const char *path);
            d = opendir(cheminActuel);

            if (d) {
                client_send(clients[i].sock, "SERVER: Liste des elements: \n");
                while ((dir = readdir(d)) != NULL) {

                    char message[1000] = "";

                    char subdir[1000] = "";
                    strcpy(subdir, cheminActuel);
                    strcat(subdir, "/");
                    strcat(subdir, dir->d_name);

                    // Je v�rifie l'�l�ment sur lequel on boucle est un dossier
                    if (testSiDossierEtSiExiste(subdir))
                    {
                        strcat(message, "SERVER: Folder: ");
                        strcat(message, dir->d_name);
                    }
                    else
                    {
                        strcat(message, "SERVER: FILE  : ");
                        strcat(message, dir->d_name);
                    }
                    strcat(message, " ");
                    printf("%s\n", dir->d_name);

                    client_send(clients[i].sock, message);
                }
                closedir(d);
            }
        }

        // Si l'utilisateur ecris 'cd'
        else if (strstr(buffer, "cd") != NULL)
        {
            char *destination = buffer;
            destination += 3;
            printf("Destination: %s \n", destination);

            if (strstr(buffer, "..") != NULL)
            {
                char str[1000];
                strcpy(str, cheminActuel);
                int    ch = '/';
                int j;
                int index;
                index = -1;

                for(j = 0; j <= strlen(str); j++)
                {
                    if(str[j] == ch)
                    {
                        index = j;
                    }
                }
                if(index == -1)
                {
                    client_send(clients[i].sock, "SERVER: ERREUR \n");
                }
                char src[1000];
                char dest[1000];
                strcpy(src, cheminActuel);
                memset(dest, '\0', sizeof(dest));
                strncpy(dest, src, index);

                if (strlen(dest) < strlen(cheminInitial))
                {
                    printf("Desole mais vous ne pouvez pas remonter plus haut que la racine, ce serais bien trop dangereux ;D");
                    client_send(clients[i].sock, "Desole mais vous ne pouvez pas remonter plus haut que la racine, ce serais bien trop dangereux ;D \n");
                }
                else
                {
                    // On r�cup�re le nom du dossier actuel
                    int index2;
                    index2 = -1;
                    for(j = 0; j <= strlen(str); j++)
                    {
                        if(dest[j] == ch)
                        {
                            index2 = j;
                        }
                    }
                    char *dossierActuel;
                    dossierActuel = strndup(dest + index2 + 1, 100);

                    // On affiche le dossier actuel
                    char message[1000] = "";
                    strcat(message, "\nDossier actuel: ");
                    strcat(message, dossierActuel);
                    client_send(clients[i].sock, message);

                    // On vas dans le dit dossier
                    cheminActuel = dest;
                }
            }
            else
            {
                char subdir[1000] = "";
                strcpy(subdir, cheminActuel);
                strcat(subdir, "/");
                strcat(subdir, destination);

                if (testSiDossierEtSiExiste(subdir))
                {

                    printf("Le dossier existe\n");
                    char message[1000] = "";

                    strcat(message, "\nDossier actuel: ");
                    strcat(message, destination);

                    client_send(clients[i].sock, message);

                    strcpy(cheminIntermediaire, subdir);
                    cheminActuel = subdir;
                    cheminActuel = cheminIntermediaire;

                    //printf("\nsubdir: %s\n", subdir);
                    //printf("cheminActuel: %s\n", cheminActuel);

                }

                else
                {
                    printf("Le dossier n'existe pas \n");
                    client_send(clients[i].sock, "Le dossier n'existe pas \n");
                }



                strcpy(subdir, "");
                destination = "";
            }
        }


        // Si l'utilisateur ecris 'mkdir'
        else if (strstr(buffer, "mkdir") != NULL)
        {
            char *destination = buffer;
            destination += 6;
            printf("Destination: %s \n", destination);

            char newPath[1000] = "";

            strcpy(newPath, cheminActuel);
            strcat(newPath, "/");
            strcat(newPath, destination);

            struct stat st = {0};
           if (stat(newPath, &st) == -1) {
                mkdir(newPath, 0700);
                client_send(clients[i].sock, "Le dossier a bien ete cree");
            }
            else
            {
                client_send(clients[i].sock, "Le dossier existe deja");
            }
        }

        // Si l'utilisateur ecris 'delete'
        else if (strstr(buffer, "delete") != NULL)
        {
            char *destination = buffer;
            destination += 7;
            printf("A supprimer: %s \n", destination);

            char newPath[1000] = "";
            strcpy(newPath, cheminActuel);
            strcat(newPath, "/");
            strcat(newPath, destination);
            printf("A supprimer: %s \n", newPath);

            if (testSiDossierEtSiExiste(newPath))
            {
                removeDirectory(newPath);
            }
            rmdir(newPath);
            client_send(clients[i].sock, "Le dossier a bien ete supprime");
        }


        // Si l'utilisateur ecris 'status'
        else if (strstr(buffer, "status") != NULL)
        {
            client_send(clients[i].sock, messageOfNumberOfConnected);
        }

        // Si l'utilisateur ecris 'put'
        else if (strstr(buffer, "put") != NULL)
        {

            // On d�coupe la chaine pour r�cup�rer le nom du fichier avec son extension
            const char *debutNom = "<name>";
            const char *finNom = "</name>";
            char *nomFinal = NULL;
            char *debut, *fin;

            if (debut = strstr(buffer, debutNom))
            {
                debut += strlen(debutNom);
                if (fin = strstr(debut, finNom))
                {
                    nomFinal = (char *)malloc(fin - debut + 1);
                    memcpy (nomFinal, debut, fin - debut);
                    nomFinal[fin - debut] = '\0';
                }
            }

            const char *debutContent = "<content>";
            const char *finContent = "</content>";

            char *contentFinal = NULL;
            char *debut2, *fin2;

            if (debut2 = strstr(buffer, debutContent))
            {
                debut2 += strlen(debutContent);
                if (fin2 = strstr(debut2, finContent))
                {
                    contentFinal = (char *)malloc(fin2 - debut2 + 1);
                    memcpy (contentFinal, debut2, fin2 - debut2);
                    contentFinal[fin2 - debut2] = '\0';
                }
            }

            char cheminNouveauFichier[1000] = "";
            strcpy(cheminNouveauFichier, cheminActuel);
            strcat(cheminNouveauFichier, "/");
            strcat(cheminNouveauFichier, nomFinal);

            FILE *fileToCreate;
            fileToCreate = fopen(cheminNouveauFichier, "w");
            fputs(contentFinal, fileToCreate);
            fclose(fileToCreate);
            free(nomFinal);
        }

        else
        {
            // Si l'utilisateur ecris 'quit'
            if (strstr(buffer, "quit") != NULL)
            {
                client_close(clients, i, &nbClients);
            }
            else
            {
                if (connectionTerminee > 0)
                {
                    client_send(clients[i].sock, "Commande Inconnue");
                }
                connectionTerminee = 1;
            }
        }
    }
    close_all(clients, &nbClients);
    return sock;
}

void stop(SOCKET sock) {
   closesocket(sock);
}


void close_all(Client *clients, int *nbClients){
   int i = 0;
   for(i = 0; i < *nbClients; i++)
   {
      client_close(clients,i, nbClients);
   }
}

void client_close(Client *clients, int i, int *nbClients){
    closesocket(clients[i].sock);
    memmove(clients + i, clients + i + 1, (*nbClients - i - 1) * sizeof(Client));
    (*nbClients)--;
    printf("%d client deja connecte\n", *nbClients);

}

void send_to_all(Client *clients, Client sender, int nbClients, const char *msg){
   int i = 0;

   for(i = 0; i < nbClients; i++)
   {
      if(sender.sock != clients[i].sock)
      {
         client_send(clients[i].sock, msg);
      }
   }
}

int client_recv(SOCKET sock, char *msg) {
   int n = 0;

   if((n = recv(sock, msg, 1024 - 1, 0)) < 0)
   {

      n = 0;
   }

   msg[n] = 0;

   return n;
}

int client_send(SOCKET sock, const char *msg) {
   return send(sock, msg, strlen(msg), 0);
}


// Fonction qui v�rifie si l'�l�ment 'path' est un dossier et existe
int testSiDossierEtSiExiste(const char *path)
{
    struct stat stats;
    stat(path, &stats);
    if (S_ISDIR(stats.st_mode))
        return 1;

    return 0;
}

int removeDirectory(const char *path)
{
    DIR           *d;
    struct dirent *dir;
    DIR           *subdir;
    d = opendir(path);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            // On r�cup�re le nom du dossier actuel
            int index2;
            int j;
            index2 = -1;
            int    ch = '.';
            for(j = 0; j <= strlen(path); j++)
            {
                if(path[j] == ch)
                {
                    index2 = j;
                }
            }
            char *dossierActuel;
            dossierActuel = strndup(path + index2 + 1, 100);

            if(strstr(dossierActuel, ".") != NULL) {
                    printf("Impossible");
            }
            else
            {
                char subdir[1000] = "";
                strcpy(subdir, path);
                strcat(subdir, "/");
                strcat(subdir, dir->d_name);

                if (testSiDossierEtSiExiste(subdir))
                {
                    printf("Subdir: %s\n", subdir);
                    removeDirectory(subdir);
                    rmdir(path);
                }
                else
                {
                    rmdir(subdir);
                }
            }
        }
        closedir(d);
    }
}
