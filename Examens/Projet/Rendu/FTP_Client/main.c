// FTP CLIENT

#include <stdio.h>
#include <stdlib.h>
//#include <errno.h>
//#include <string.h>

#include "client.h"

int main(int argc, char **argv)
{
	printf("FTP - CLIENT!\n");
    char host[255];
    printf("open ");
    scanf("%s", &host);
    printf("identifiant ");
    char name[255];
    scanf("%s", &name);

    SOCKET sock = init(host);

    start(sock, name);
    stop(sock);

    finish();

    return EXIT_SUCCESS;
}
