#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    // Message d'accueil.
    printf("\n\nSupCRYPT est un programme qui vous permet d'encrypter un fichier.\n");

    // On initialise la variable qui va stoquer le nom du fichier d'entr�e.
    char nomDuFichier[100];

    char nomDuFichierDeSortie[100], caractereActuel;

    // On stocke dans fichierOuvert l'�tat d'ouverture du fichier (utile pour la gestion d'erreurs � l'ouverture)
    int fichierOuvert = 0;
    int fichierSortieOuvert = 0;

    int finEncryptage = 0;

    char isEncrypted[5];

    FILE *inputFile, *outputFile;

    printf("Est-ce que le fichier est crypter ? (oui ou non)\n");
    scanf("%s", isEncrypted);

    // Tant que le fichier n'est pas ouvert/erreur alors on redemande � l'utilisateur
    while (fichierOuvert == 0)
    {
        // On demande � l'utilisateur le chemin du fichier.
        printf("\nQuel est le nom du fichier (chemin absolu) (vous pouvez aussi faire glisser le fichier dans le terminal) \?\n");
        printf("ATTENTION: Pas d'espaces � la fin du chemin ou de la commande!!!\n");
        // On stocke le chemin dans la variable nomDuFichier.
        scanf("%s", nomDuFichier);





        // On ouvre le fichier en lecture seule et on le stocke dans inputFile
        inputFile = fopen(nomDuFichier, "r");

        if (inputFile == NULL)  // Si on a une erreur
        {
            printf("Il y a eu une erreur � l'ouverture du fichier");
            fclose(inputFile);
        }
        else    // Si on a pas d'erreurs
        {
            fichierOuvert = 1;
        }
    }





    // Tant que le fichier n'est pas ouvert/erreur alors on redemande � l'utilisateur
    while (fichierSortieOuvert == 0)
    {

        // On ouvre le fichier en ecriture et on le stocke dans outputFile
        outputFile = fopen("output.txt", "w");



        if (outputFile == NULL)  // Si on a une erreur
        {
            printf("Il y a eu une erreur a la creation du fichier");
            fclose(outputFile);
        }
        else    // Si on a pas d'erreurs
        {
            fichierSortieOuvert = 1;
        }
    }

    // Ici on est s�r que les fichiers sont ouverts

    // Si le fichier n'est pas encrypter
    if (strcmp(isEncrypted, "oui"))
    {
        printf("coucou\n");
        // Tant que l'on a pas finis l'encryptage
        while(finEncryptage == 0)
        {
            caractereActuel = fgetc(inputFile);
            if(caractereActuel == EOF)
            {
                finEncryptage = 1;
            }
            else
            {
                caractereActuel = caractereActuel + 100;
                fputc(caractereActuel, outputFile);
            }
        }
    }
    // Si le fichier est encrypter
    else
    {
        while(finEncryptage = 0)
        {
            caractereActuel = fgetc(inputFile);
            if(caractereActuel == EOF)
            {
                finEncryptage = 1;
            }
            else
            {
                caractereActuel = caractereActuel - 100;
                fputc(caractereActuel, outputFile);
            }
        }
    }


    printf("Hello world!\n");
    return 0;
}
