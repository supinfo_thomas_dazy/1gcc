#include "client.h"

SOCKET init(char *address){
    #ifdef WIN32
    WSADATA wsa;
    int err = WSAStartup(MAKEWORD(2, 2), &wsa);
    if(err < 0){
        printf("fail to load dll\n");
        exit(-1);
    }
    #endif
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    SOCKADDR_IN serv_addr;
    struct hostent *host;

    host = gethostbyname(address);
    if (host == NULL) {
        printf("Unknown host %s.\n", address);
        exit(-1);
    }

    serv_addr.sin_addr = *(IN_ADDR *) host->h_addr;
    serv_addr.sin_port = htons(1337);
    serv_addr.sin_family = AF_INET;

    if(connect(sock,(SOCKADDR *) &serv_addr, sizeof(SOCKADDR)) < 0){
        printf("connect() fail\n");
        exit(-1);
    }
    return sock;
}

void finish(void){
    #ifdef WIN32
        WSACleanup();
    #endif
}

void start(SOCKET sock, char *name){

    char buffer[1024];

    fd_set fs;

    /* send our name */
    server_send(sock, name);
    char *interrupt = NULL;
    while(1){

        FD_ZERO(&fs);

        FD_SET(STDIN_FILENO, &fs);

        FD_SET(sock, &fs);

        if(select(sock + 1, &fs, NULL, NULL, NULL) < 0){
            printf("Select error\n");
        }

        /* something from standard input : i.e keyboard */
        if(FD_ISSET(STDIN_FILENO, &fs)){

            fgets(buffer, 1024 - 1, stdin) ;
            //{
                //char *p = NULL;
                interrupt = strstr(buffer, "\n");
                if(interrupt != NULL) {
                    *interrupt = 0;
                }
                //else {
                //    /* fclean */
                //    buffer[1024 - 1] = 0;
                //}
            //}
            server_send(sock, buffer);
        }
        else if(FD_ISSET(sock, &fs)){
            //buffer[1024 - 1] = 0;
            if(server_recv(sock, buffer) == 0){

                printf("Server disconnected !\n");
                break;
            }
            puts(buffer);

        }
    }

}


void stop(SOCKET sock){
    closesocket(sock);
}

int server_recv(SOCKET sock, char *buffer){
    int n = 0;

   if((n = recv(sock, buffer, 1024 - 1, 0)) < 0)
   {


      n = 0;
   }

   buffer[n] = 0;

   return n;
}

void server_send(SOCKET sock, char *buffer){
   send(sock, buffer, strlen(buffer), 0);
}
