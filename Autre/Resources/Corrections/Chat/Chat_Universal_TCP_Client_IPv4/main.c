#include <stdio.h>
#include <stdlib.h>
//#include <errno.h>
//#include <string.h>

#include "client.h"

int main(int argc, char **argv)
{
	printf("Welcome to Supinfo in the C language!\n");
    char host[255];
    printf("Address : ");
    scanf("%s", &host);
    printf("Your pseudo : ");
    char name[255];
    scanf("%s", &name);

    SOCKET sock = init(host);

    start(sock, name);
    stop(sock);

    finish();

    return EXIT_SUCCESS;
}
