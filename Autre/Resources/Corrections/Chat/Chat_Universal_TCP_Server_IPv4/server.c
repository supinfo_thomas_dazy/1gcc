#include "server.h"


SOCKET init(void){
    #ifdef WIN32
        WSADATA wsa;
        int err = WSAStartup(MAKEWORD(2, 2), &wsa);
        if(err < 0){
            printf("Load Dll fail\n");
            exit(EXIT_FAILURE);
        }
    #endif
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    SOCKADDR_IN serv_addr;

    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(1337);
    serv_addr.sin_family = AF_INET;

    bind(sock,(SOCKADDR *) &serv_addr, sizeof(serv_addr));
    return sock;
}

void finish(void){
    #ifdef WIN32
        WSACleanup();
    #endif
}

SOCKET start(SOCKET sock) {
    if(listen(sock, 100) < 0) {
        printf("Open port fail\n");
        exit(-1);
    }
    char buffer[1024],tmpBuffer[1024];
    /* the index for the array */
    int nbClients = 0;//nb of client
    int set = 0;//select result
    int max = sock;
    int i;

    Client clients[100];

    fd_set fs;

    while(1) {
        i = 0;

        FD_ZERO(&fs);
        FD_SET(sock, &fs);

        for(i = 0; i < nbClients; i++){
            FD_SET(clients[i].sock, &fs);
        }

        set = select(max + 1, &fs, NULL, NULL, NULL);
        if(set < 0){
            printf("Select error\n");
        }
        else if(set  == 0){
            //Time out not set in this example
        }

        else if(FD_ISSET(sock, &fs)){
            SOCKADDR_IN cli_addr;
            int sinsize = sizeof(cli_addr);
            SOCKET cli = accept(sock, (SOCKADDR *) &cli_addr, &sinsize);

            Client c;
            c.sock = cli;
            if(client_recv(cli, c.name) < 0){
                /* disconnected */
            }
            printf("Connected: %s\n", c.name);

            max = cli > max ? cli : max;

            FD_SET(cli, &fs);

            clients[nbClients] = c;

            strncpy(tmpBuffer, "Server: ",sizeof(tmpBuffer));
            strncat(tmpBuffer, clients[nbClients].name, sizeof(tmpBuffer) - strlen(tmpBuffer) - 1);
            strncat(tmpBuffer, " is now connected", sizeof(tmpBuffer) - strlen(tmpBuffer) - 1);

            send_to_all(clients,clients[nbClients], nbClients, tmpBuffer);
            nbClients++;
            printf("%d clients connected\n",nbClients);
        }
        else{
            for(i = 0; i < nbClients; i++){
                if(FD_ISSET(clients[i].sock, &fs)){

                    if(client_recv(clients[i].sock, buffer) < 1){
                        printf("Disconected: %s \n",clients[i].name);

                        strncpy(buffer, clients[i].name, sizeof(buffer) );
                        strncat(buffer, " disconnected !!!", sizeof(buffer) - strlen(buffer) - 1);

                        send_to_all(clients, clients[i], nbClients, buffer);
                        client_close(clients, i, &nbClients);
                    }
                    else{
                        printf("%s: %s\n", clients[i].name, buffer);
                        strncpy(tmpBuffer, clients[i].name, sizeof(tmpBuffer));
                        strncat(tmpBuffer, " : ", sizeof(tmpBuffer) - strlen(tmpBuffer) - 1);
                        strncat(tmpBuffer, buffer, sizeof(tmpBuffer) - strlen(tmpBuffer) - 1);
                        send_to_all(clients, clients[i], nbClients, tmpBuffer);
                    }
                    break;
                }
            }
        }
    }
    close_all(clients, &nbClients);
    return sock;
}

void stop(SOCKET sock) {
   closesocket(sock);
}


void close_all(Client *clients, int *nbClients){
   int i = 0;
   for(i = 0; i < *nbClients; i++)
   {
      client_close(clients,i, nbClients);
   }
}

void client_close(Client *clients, int i, int *nbClients){
    closesocket(clients[i].sock);
    memmove(clients + i, clients + i + 1, (*nbClients - i - 1) * sizeof(Client));
    (*nbClients)--;
    printf("%d clients already connected\n", *nbClients);

}


void send_to_all(Client *clients, Client sender, int nbClients, const char *msg){
   int i = 0;

   for(i = 0; i < nbClients; i++)
   {
      if(sender.sock != clients[i].sock)
      {
         client_send(clients[i].sock, msg);
      }
   }
}




int client_recv(SOCKET sock, char *msg) {
   int n = 0;

   if((n = recv(sock, msg, 1024 - 1, 0)) < 0)
   {

      n = 0;
   }

   msg[n] = 0;

   return n;
}

int client_send(SOCKET sock, const char *msg) {
   return send(sock, msg, strlen(msg), 0);
}
