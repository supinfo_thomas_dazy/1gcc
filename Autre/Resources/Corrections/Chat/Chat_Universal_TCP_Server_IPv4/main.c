#include <stdio.h>
#include <stdlib.h>

#include "server.h"


int main(int argc, char **argv)
{

	printf("Welcome to Supinfo in the C language!\n");
    SOCKET sock = init();

    sock = start(sock);
    stop(sock);

    finish();

   return EXIT_SUCCESS;
}
