#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>

int main()
{
    printf("Welcome to Supinfo in the C language!\n");

    WSADATA wsa;
	WSAStartup(MAKEWORD(2, 2), &wsa);

    SOCKADDR_IN serv_addr;
    SOCKET sock;
    char buffer[1024] = {0};

    //to use TCP/IPV4
    serv_addr.sin_family = AF_INET;
    //to use the port 1337
    serv_addr.sin_port = htons(1337);
    //to use localhost server
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");


    //connect to server
    sock = socket(AF_INET, SOCK_STREAM, 0);
    connect(sock, (SOCKADDR *) &serv_addr, sizeof(SOCKADDR));

    //send message
    printf("Enter a message to send: ");
    scanf("%s",&buffer);
    send(sock, buffer, sizeof(buffer), 0);

    recv(sock, buffer, sizeof(buffer), 0);
    printf("response: %s\n", buffer);

    //close connection
    closesocket(sock);

    WSACleanup();



    return 0;
}
