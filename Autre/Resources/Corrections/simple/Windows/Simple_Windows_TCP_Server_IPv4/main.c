#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>


int main()
{
    printf("Welcome to Supinfo in the C language!\n");

    WSADATA wsa;
	WSAStartup(MAKEWORD(2, 2), &wsa);

    SOCKADDR_IN serv_addr, cli_addr;
    SOCKET sock, cli_sock;
    char buffer[1024] = {0};


    //to use TCP/IPV4
    serv_addr.sin_family = AF_INET;
    //to use the port 1337
    serv_addr.sin_port = htons(1337);
    //to use all network interface
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    //bind server
    sock = socket(AF_INET, SOCK_STREAM, 0);
    bind(sock, (SOCKADDR *) &serv_addr, sizeof(serv_addr));
    listen(sock, 5);

    //accept en receive message
    int clilen = sizeof(cli_addr);
    cli_sock = accept(sock,(SOCKADDR *) &cli_addr,&clilen);
    recv(cli_sock, buffer, sizeof(buffer), 0);
    printf("msg receive : %s \n",buffer);
    send(cli_sock, "Message receive!", sizeof(buffer), 0);

    //close client and server
    closesocket(cli_sock);
    closesocket(sock);

    WSACleanup();

    return 0;
}
