#include<stdio.h>
#include<stdlib.h>

main()
{
    char fn[255], nfn[255];
    int ch;
    long int size, n, k;
    int i;
    FILE *f, *ft; //file and temp file

    printf("enter the file you want to split with full path : ");
    scanf("%s", fn);


    f=fopen(fn, "rb");
    if (f==NULL)
    {
        printf("couldn't open file");
        exit(0);
    }

    fseek(f, 0, 2);
    size = ftell(f);
    printf("the size of the file in bytes is : %ld\n", size);
    printf("enter the size of  parts that you want (Mb): ");
    scanf("%ld", &n);
    i = 1;
    k = n*1024*1024;
    rewind(f);
    sprintf(nfn, "%s.%d", fn, i);
    ft = fopen(nfn, "wb");
    while(1)
    {
        ch = fgetc(f);
        if (ch==EOF){
            fclose(ft);
            break;
        }
        fputc(ch, ft);
        if (ftell(f)==(i*k)){
            i++;
            fclose(ft);
            sprintf(nfn, "%s.%d", fn, i);
            ft=fopen(nfn, "wb");
        }
    }
    fclose(f);
}
